﻿using System.Xml.Serialization;
using System.Collections.Generic;
using CityDataExpansionModule.ObjectsXml;

namespace CityDataExpansionModule
{
    [XmlRoot(ElementName = "osm")]
    public class OsmXml
    {
        [XmlElement(ElementName = "bounds")]
        public BoundsXml Bounds { get; set; }
        [XmlElement(ElementName = "node")]
        public List<NodeXml> Node { get; set; }
        [XmlElement(ElementName = "way")]
        public List<WayXml> Way { get; set; }
        [XmlElement(ElementName = "relation")]
        public List<RelationXml> Relation { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "generator")]
        public string Generator { get; set; }
        [XmlAttribute(AttributeName = "copyright")]
        public string Copyright { get; set; }
        [XmlAttribute(AttributeName = "attribution")]
        public string Attribution { get; set; }
        [XmlAttribute(AttributeName = "license")]
        public string License { get; set; }
    }
}
