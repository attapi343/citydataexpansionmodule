﻿using System;
using OSMLSGlobalLibrary.Modules;
using System.Linq;
using System.Collections.Generic;
using NetTopologySuite.Geometries;
using System.Xml.Serialization;
using System.IO;
using CityDataExpansionModule.OsmGeometries;
using Newtonsoft.Json;
using CityDataExpansionModule.Configuration;

namespace CityDataExpansionModule
{
    [CustomInitializationOrder(-10000)]
    public class CityDataExpansion : OSMLSModule
    {
        /// <summary>
        /// Все здания
        /// </summary>
        public IReadOnlyCollection<OsmClosedWay> AllBuildings =>
            MapObjects.GetAll<OsmClosedWay>().Where(x => x.Tags.ContainsKey("building")).ToList().AsReadOnly();

        /// <summary>
        /// Рельсовые пути
        /// </summary>
        public IReadOnlyCollection<OsmWay> AllRailways =>
            MapObjects.GetAll<OsmWay>().Where(x => x.Tags.ContainsKey("railway")).ToList().AsReadOnly();
        /// <summary>
        /// Велодорожки
        /// </summary>
        public IReadOnlyCollection<OsmWay> AllCycleways =>
            MapObjects.GetAll<OsmWay>().Where(x => x.Tags.ContainsKey("cycleway")).ToList().AsReadOnly();
        /// <summary>
        /// Границы участка карты
        /// </summary>
        public Polygon MapBorders { get; private set; }


        protected override void Initialize()
        {
            List<IOsmObject> osmObjects;
            OsmXml rawData;
            var filePath = $"{AppContext.BaseDirectory}/settings/generatorConfig.json";
           
            using (var streamReader = new StreamReader(OsmFilePath))
            {
                rawData = (OsmXml)new XmlSerializer(typeof(OsmXml)).Deserialize(streamReader);
            }
 
            (MapBorders, osmObjects) = new OsmObjectsConverter().ConvertObjectsOSM(rawData);

            var generatorConfiguration = GeneratorConfiguration.CreateDemo();
            if (File.Exists(filePath))
            {
                generatorConfiguration = Deserialize(filePath);
            }
            else
            {
                Serialize(filePath, generatorConfiguration);
            }
            new OsmObjectsGenerator().Generate(osmObjects, generatorConfiguration);

            foreach (var osmGeometry in osmObjects.OfType<Geometry>())
            {
                MapObjects.Add(osmGeometry);
            }
        }
        public override void Update(long elapsedMilliseconds) { }

        public void Serialize(string filePath, GeneratorConfiguration generatingConfiguration)
        {
            //сериализовать JSON в строку
            var json = JsonConvert.SerializeObject(generatingConfiguration, Formatting.Indented);

            // запись строки в файл
            File.WriteAllText(filePath, json);
        }
        public GeneratorConfiguration Deserialize(string filePath)
        {
            var generatingConfiguration = JsonConvert.DeserializeObject<GeneratorConfiguration>(File.ReadAllText(filePath));
            return generatingConfiguration;
        }
    }
}