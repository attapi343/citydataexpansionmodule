﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace CityDataExpansionModule.OsmGeometries
{
    public class OsmClosedWay : LinearRing, IOsmObject
    {
        public Dictionary<string, string> Tags { get; }

        private readonly OsmNode[] _nodes;

        public OsmNode[] Nodes => _nodes.ToArray();

        public OsmClosedWay(OsmNode[] nodes, Dictionary<string, string> tags = null) : base(nodes.Select(node => node.Coordinate).ToArray())
        {
            Tags = tags ?? new Dictionary<string, string>();
            _nodes = nodes;
        }

        public Polygon ToPolygon => new Polygon(this);

        public override bool Equals(object o)
        {
            if (!(o is IOsmObject))
                return false;

            var otherOsmObject = (IOsmObject)o;
            return base.Equals(o) && Tags.ContentAreEqual(otherOsmObject.Tags);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Tags.GetHashCode();
        }
    }
}
