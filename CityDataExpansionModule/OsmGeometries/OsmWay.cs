﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace CityDataExpansionModule.OsmGeometries
{
    public class OsmWay : LineString, IOsmObject
    {
        public Dictionary<string, string> Tags { get; }

        private readonly OsmNode[] _nodes;

        public OsmNode[] Nodes => _nodes.ToArray();

        public OsmWay(OsmNode[] nodes, Dictionary<string, string> tags = null) : base(nodes.Select(node => node.Coordinate).ToArray())
        {
            Tags = tags ?? new Dictionary<string, string>();
            _nodes = nodes;
        }

        public override bool Equals(object o)
        {
            if (o == null || !(o is IOsmObject))
                return false;

            var otherOsmObject = (IOsmObject)o;
            return base.Equals(o) && Tags.ContentAreEqual(otherOsmObject.Tags);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Tags.GetHashCode();
        }
    }
}
