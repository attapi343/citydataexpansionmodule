﻿using System.Collections.Generic;

namespace CityDataExpansionModule.OsmGeometries
{
    public interface IOsmObject
    {
        Dictionary<string, string> Tags { get; }
    }
}
