﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;

namespace CityDataExpansionModule.OsmGeometries
{
    public class OsmNode : Point, IOsmObject
    {
        public Dictionary<string, string> Tags { get; }

        public OsmNode(Coordinate coordinate, Dictionary<string, string> tags = null) : base(coordinate)
        {
            Tags = tags ?? new Dictionary<string, string>();
        }

        public override bool Equals(object o)
        {
            if (!(o is IOsmObject))
                return false;

            var otherOsmObject = (IOsmObject)o;
            return base.Equals(o) && Tags.ContentAreEqual(otherOsmObject.Tags);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Tags.GetHashCode();
        }
    }
}
