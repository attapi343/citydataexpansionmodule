﻿using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace CityDataExpansionModule.OsmGeometries
{
    public class OsmRelation: GeometryCollection, IOsmObject
    {
        private readonly string[] _roles;

        public Dictionary<string, string> Tags { get; }

        public string GetRole(int index)
        {
            return _roles[index];
        }

        public void SetRole(int index, string role)
        {
            _roles[index] = role;
        }

        public OsmRelation(List<(Geometry geometry, string role)> geometriesToRoles, Dictionary<string, string> tags = null) : base(geometriesToRoles.Select(x => x.geometry).ToArray()) {
            Tags = tags ?? new Dictionary<string, string>();
            _roles = geometriesToRoles.Select(x => x.role).ToArray();
        }
        public override bool Equals(object o)
        {
            if (!(o is IOsmObject))
                return false;

            var otherOsmObject = (IOsmObject)o;
            return base.Equals(o) && Tags.ContentAreEqual(otherOsmObject.Tags);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ Tags.GetHashCode();
        }
    }
}
