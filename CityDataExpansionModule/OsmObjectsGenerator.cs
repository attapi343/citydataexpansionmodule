﻿using CityDataExpansionModule.Configuration;
using CityDataExpansionModule.OsmGeometries;
using NetTopologySuite.Geometries;
using System.Collections.Generic;
using System.Linq;

namespace CityDataExpansionModule
{
    public class OsmObjectsGenerator
    {
        public void Generate(List<IOsmObject> osmObjects, GeneratorConfiguration generatorConfiguration)
        {
            if (generatorConfiguration.IsEntrancesGenerationEnabled)
            {
                GenerationEntrances(osmObjects, generatorConfiguration.BuildingsTypesToEntrancesCount, generatorConfiguration.DefaultEntrancesCount);
            }
            if (generatorConfiguration.IsLevelsGenerationEnabled)
            {
                GenerationLevels(osmObjects, generatorConfiguration.BuildingsTypesToLevelsCount, generatorConfiguration.DefaultLevelsCount);
            }
            if (generatorConfiguration.IsFlatsGenerationEnabled)
            {
                GenerationApartments(osmObjects, generatorConfiguration.BuildingsTypesToFlatsCount, generatorConfiguration.DefaultFlatsCountForEveryEntranceLevels);
            }
        }

        private static void GenerationEntrances(List<IOsmObject> osmObjects, Dictionary<int, List<string>> buildingsTypesToEntrancesCount, int defaultEntrancesCount)
        {
           
            var osmAllWay = osmObjects.OfType<OsmClosedWay>().ToList();

            var buildingsWithoutAnyEntrance = osmAllWay
                .Where(x => x.Tags.ContainsKey("building"))
                .Where(w => !w.Nodes.Any(n => n.Tags.ContainsKey("entrance")));

            foreach (var buildingWithoutAnyEntrance in buildingsWithoutAnyEntrance.ToList())
            {
                osmObjects.Remove(buildingWithoutAnyEntrance);

                var buildingLines = new List<LineSegment>();
                for (var i = 1; i < buildingWithoutAnyEntrance.NumPoints; i++)
                {
                    buildingLines.Add(
                        new LineSegment(buildingWithoutAnyEntrance[i - 1], buildingWithoutAnyEntrance[i])
                        );
                }

                var facade = buildingLines.OrderBy(x => x.Length).Last();

                var entrancesCount = defaultEntrancesCount; // кол-во подъездов 
                foreach (var buildingTypesToEntrancesCount in buildingsTypesToEntrancesCount)
                {
                    if (buildingTypesToEntrancesCount.Value.Contains(buildingWithoutAnyEntrance.Tags["building"]))
                    {
                        entrancesCount = buildingTypesToEntrancesCount.Key;
                        break;
                    }
                }

                var newBuildingNodes = new List<OsmNode>();
                var firstPointAchived = false;
                foreach (var node in buildingWithoutAnyEntrance.Nodes)
                {
                    if (node.Coordinate == facade.P0)
                    {
                        firstPointAchived = true;
                    }
                    if (node.Coordinate == facade.P1 && firstPointAchived)
                    {
                        var partsCount = entrancesCount + 1;
                        for (var i = 1; i < partsCount; i++)
                        {
                            var t = (double)i / partsCount;
                            var entranceNode = new OsmNode(new Coordinate(
                                facade.P0.X * (1 - t) + facade.P1.X * t,
                                facade.P0.Y * (1 - t) + facade.P1.Y * t
                            ), new Dictionary<string, string>() { { "entrance", "yes" } });

                            osmObjects.Add(entranceNode);
                            newBuildingNodes.Add(entranceNode);
                        }
                    }
                    newBuildingNodes.Add(node);
                }

                osmObjects.Add(
                    new OsmClosedWay(
                        newBuildingNodes.ToArray(),
                        buildingWithoutAnyEntrance.Tags
                        )
                    );
            }
        }

        private static void GenerationLevels(List<IOsmObject> osmObjects, Dictionary<int, List<string>> buildingsTypesToLevelsCount, int defaultLevelsCount)
        {

            var osmAllWay = osmObjects.OfType<OsmClosedWay>().ToList();

            var buildingsWithoutLevels = osmAllWay
                .Where(x => x.Tags.ContainsKey("building"))
                .Where(y => !y.Tags.ContainsKey("building:levels"));

            foreach (var buildingWithoutLevels in buildingsWithoutLevels)
            {
                foreach (var buildingTypeToLevelsCount in buildingsTypesToLevelsCount)
                {
                    if (buildingTypeToLevelsCount.Value.Contains(buildingWithoutLevels.Tags["building"]))
                    {
                        buildingWithoutLevels.Tags["building:levels"] = buildingTypeToLevelsCount.Key.ToString();
                        break;
                    }
                }
                if (!buildingWithoutLevels.Tags.ContainsKey("building:levels")) // Если этажность не была задана в словаре
                {
                    // Берем значение этажности у ближайшего здания или значение по-умолчнию
                    buildingWithoutLevels.Tags["building:levels"] = osmObjects
                        .Where(x => x.Tags.ContainsKey("building"))
                        .Where(x => x.Tags.ContainsKey("building:levels"))
                        .OrderBy(x => buildingWithoutLevels.Distance((Geometry)x))
                        .FirstOrDefault()?.Tags["building:levels"] ?? defaultLevelsCount.ToString();
                }
            }

            var buildingsWithLevels = osmAllWay
              .Where(x => x.Tags.ContainsKey("building"))
              .Where(y => y.Tags.ContainsKey("building:levels"));

            foreach (var buildingWithLevels in buildingsWithLevels)
            {
                if (buildingWithLevels.Tags["building:levels"] == "0")
                {
                    buildingWithLevels.Tags["building:levels"] = "1";
                }
            }
        }

        private static void GenerationApartments(List<IOsmObject> osmObjects, Dictionary<int, List<string>> buildingsTypesToFlatsCount, int defaultApartmentsCountForEveryEntranceLevels)
        {
            var osmAllWay = osmObjects.OfType<OsmClosedWay>().ToList();

            var buildingsWithoutFlats = osmAllWay
                .Where(x => x.Tags.ContainsKey("building"))
                .Where(x => !x.Tags.ContainsKey("building:flats"))
                .Where(x => x.Tags.ContainsKey("building:levels"))
                .Where(x => x.Nodes.Any(n => n.Tags.ContainsKey("entrance")));

            foreach (var buildingWithoutFlats in buildingsWithoutFlats)
            {
                int entranceCount = buildingWithoutFlats.Nodes.Count(n => n.Tags.ContainsKey("entrance"));
                int levelsCount = int.Parse(buildingWithoutFlats.Tags["building:levels"]);
                buildingWithoutFlats.Tags["building:flats"] = (entranceCount * levelsCount * defaultApartmentsCountForEveryEntranceLevels).ToString();

                foreach (var buildingTypesToFlatsCount in buildingsTypesToFlatsCount)
                {
                    if (buildingTypesToFlatsCount.Value.Contains(buildingWithoutFlats.Tags["building"]))
                    {
                        buildingWithoutFlats.Tags["building:flats"] = (entranceCount * levelsCount * buildingTypesToFlatsCount.Key).ToString();
                        break;
                    }
                }
            }
        }
    }
}
