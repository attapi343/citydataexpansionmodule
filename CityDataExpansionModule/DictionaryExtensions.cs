﻿using System.Collections.Generic;
using System.Linq;

namespace CityDataExpansionModule
{
    public static class DictionaryExtensions
    {
        public static bool ContentAreEqual<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, Dictionary<TKey, TValue> secondDictionary)
        {
            return dictionary.Count == secondDictionary.Count && !dictionary.Except(secondDictionary).Any();
        }
    }
}
