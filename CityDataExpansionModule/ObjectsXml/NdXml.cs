﻿using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "nd")]
    public class NdXml
    {
        [XmlAttribute(AttributeName = "ref")]
        public string Ref { get; set; }
    }
}
