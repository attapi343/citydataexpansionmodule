﻿using System.Xml.Serialization;
using System.Collections.Generic;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "way")]
    public class WayXml : CommonOsmObjectXml
    {
        [XmlElement(ElementName = "nd")]
        public List<NdXml> Nd { get; set; }
    }
}
