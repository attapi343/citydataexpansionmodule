﻿using System.Xml.Serialization;
using System.Collections.Generic;


namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "relation")]
    public class RelationXml : CommonOsmObjectXml
    {
        [XmlElement(ElementName = "member")]
        public List<MemberXml> Member { get; set; }
    }
}
