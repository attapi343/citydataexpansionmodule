﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    public class CommonOsmObjectXml
    {
        [XmlElement(ElementName = "tag")]
        public List<TagXml> Tag { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "visible")]
        public string Visible { get; set; }
        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
        [XmlAttribute(AttributeName = "changeset")]
        public string Changeset { get; set; }
        [XmlAttribute(AttributeName = "timestamp")]
        public string Timestamp { get; set; }
        [XmlAttribute(AttributeName = "user")]
        public string User { get; set; }
        [XmlAttribute(AttributeName = "uid")]
        public string Uid { get; set; }
    }
}
