﻿using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "node")]
    public class NodeXml : CommonOsmObjectXml
    {
        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }
        [XmlAttribute(AttributeName = "lon")]
        public string Lon { get; set; }
    }
}
