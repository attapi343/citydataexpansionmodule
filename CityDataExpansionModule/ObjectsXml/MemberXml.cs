﻿using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "member")]
    public class MemberXml
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "ref")]
        public string Ref { get; set; }
        [XmlAttribute(AttributeName = "role")]
        public string Role { get; set; }
    }
}
