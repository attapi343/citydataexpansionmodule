﻿using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "bounds")]
    public class BoundsXml
    {
        [XmlAttribute(AttributeName = "minlat")]
        public string Minlat { get; set; }
        [XmlAttribute(AttributeName = "minlon")]
        public string Minlon { get; set; }
        [XmlAttribute(AttributeName = "maxlat")]
        public string Maxlat { get; set; }
        [XmlAttribute(AttributeName = "maxlon")]
        public string Maxlon { get; set; }
    }
}
