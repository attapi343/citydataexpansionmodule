﻿using System.Xml.Serialization;

namespace CityDataExpansionModule.ObjectsXml
{
    [XmlRoot(ElementName = "tag")]
    public class TagXml
    {
        [XmlAttribute(AttributeName = "k")]
        public string K { get; set; }
        [XmlAttribute(AttributeName = "v")]
        public string V { get; set; }
    }
}
