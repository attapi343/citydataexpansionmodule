﻿using CityDataExpansionModule.ObjectsXml;
using CityDataExpansionModule.OsmGeometries;
using NetTopologySuite.Geometries;
using OSMLSGlobalLibrary;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CityDataExpansionModule
{
    public class OsmObjectsConverter
    {
        private static T SetTags<T>(T geometry, List<TagXml> rawTags) where T : Geometry, IOsmObject
        {
            foreach (var tag in rawTags)
            {
                geometry.Tags[tag.K] = tag.V;
            }

            return geometry;
        }

        public (Polygon mapBorders, List<IOsmObject> osmObjects) ConvertObjectsOSM(OsmXml OSMData)
        {
            var boundsMinLatLon = MathExtensions.LatLonToSpherMerc(
                    double.Parse(OSMData.Bounds.Minlat, NumberStyles.Any, CultureInfo.InvariantCulture),
                    double.Parse(OSMData.Bounds.Minlon, NumberStyles.Any, CultureInfo.InvariantCulture)
            );
            var boundsMaxLatLon = MathExtensions.LatLonToSpherMerc(
                   double.Parse(OSMData.Bounds.Maxlat, NumberStyles.Any, CultureInfo.InvariantCulture),
                   double.Parse(OSMData.Bounds.Maxlon, NumberStyles.Any, CultureInfo.InvariantCulture)
            );

            var mapBorders = new LinearRing(new[]
            {
                boundsMinLatLon,
                new Coordinate(boundsMinLatLon.X, boundsMaxLatLon.Y),
                boundsMaxLatLon,
                new Coordinate(boundsMaxLatLon.X, boundsMinLatLon.Y),
                boundsMinLatLon
            });

            var mapNodes = OSMData.Node.ToDictionary(
                node => node.Id,
                node =>
                {
                    var coordinate = MathExtensions.LatLonToSpherMerc(
                        double.Parse(node.Lat, NumberStyles.Any, CultureInfo.InvariantCulture),
                        double.Parse(node.Lon, NumberStyles.Any, CultureInfo.InvariantCulture)
                        );

                    return SetTags(new OsmNode(coordinate), node.Tag);
                }
            );

            var mapWays = OSMData.Way.Where(way => way.Nd.First().Ref != way.Nd.Last().Ref).ToDictionary(
                way => way.Id,
                way =>
                {
                    List<OsmNode> wayNodes = new List<OsmNode>();
                    foreach (var nodeId in way.Nd.Select(x => x.Ref))
                    {
                        //Если точка есть в словаре

                        if (mapNodes.ContainsKey(nodeId))
                        {
                            wayNodes.Add(mapNodes[nodeId]);
                        }
                    }

                    return SetTags(new OsmWay(wayNodes.ToArray()), way.Tag);
                });

            var mapAreas = OSMData.Way.Where(way => way.Nd.First().Ref == way.Nd.Last().Ref).ToDictionary(
                way => way.Id,
                way =>
                {
                    var wayNodes = new List<OsmNode>();
                    foreach (var nodeId in way.Nd.Select(x => x.Ref))
                    {
                        //Если точка есть в словаре

                        if (mapNodes.ContainsKey(nodeId))
                        {
                            wayNodes.Add(mapNodes[nodeId]);
                        }
                    }

                    return SetTags(new OsmClosedWay(wayNodes.ToArray()), way.Tag);
                });

            var mapRelations = new Dictionary<string, OsmRelation>();
            foreach (var relation in OSMData.Relation)
            {
                mapRelations[relation.Id] = SetTags(new OsmRelation(
                    relation.Member.Select(
                        member =>
                        {
                            Geometry geometry = null;

                            if (member.Type == "node")
                            {
                                if (mapNodes.ContainsKey(member.Ref))
                                    geometry = mapNodes[member.Ref];
                            }
                            else if (member.Type == "way")
                            {
                                if (mapWays.ContainsKey(member.Ref))
                                    geometry = mapWays[member.Ref];
                                else if (mapAreas.ContainsKey(member.Ref))
                                    geometry = mapAreas[member.Ref];
                            }
                            else if (member.Type == "relation")
                            {
                                if (mapRelations.ContainsKey(member.Ref))
                                    geometry = mapRelations[member.Ref];
                            }

                            return (geometry, member.Role);
                        }).Where(x => x.geometry != null).ToList()
                    ), relation.Tag);
            }

            return (
                new Polygon(mapBorders),
                mapNodes.Values.Cast<IOsmObject>()
                    .Union(mapWays.Values)
                    .Union(mapAreas.Values)
                    .Union(mapRelations.Values)
                    .ToList()
            );
        }
    }
}
