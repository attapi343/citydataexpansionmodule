﻿using System.Collections.Generic;

namespace CityDataExpansionModule.Configuration
{
    public class GeneratorConfiguration
    {
        public Dictionary<int, List<string>> BuildingsTypesToLevelsCount { get; set; } = new Dictionary<int, List<string>>();
        public int DefaultLevelsCount { get; set; }
        public bool IsLevelsGenerationEnabled { get; set; }
        public Dictionary<int, List<string>> BuildingsTypesToEntrancesCount { get; set; } = new Dictionary<int, List<string>>();
        public int DefaultEntrancesCount { get; set; }
        public bool IsEntrancesGenerationEnabled { get; set; }
        public Dictionary<int, List<string>> BuildingsTypesToFlatsCount { get; set; } = new Dictionary<int, List<string>>();
        public int DefaultFlatsCountForEveryEntranceLevels { get; set; }
        public bool IsFlatsGenerationEnabled { get; set; }

        public static GeneratorConfiguration CreateDemo()
        {
            return new GeneratorConfiguration()
            {
                DefaultLevelsCount = 5,
                DefaultEntrancesCount = 2,
                DefaultFlatsCountForEveryEntranceLevels = 3,

                BuildingsTypesToLevelsCount = new Dictionary<int, List<string>>()
            {
                { 1,
                    new List<string>{ "garage", "garages", "roof", "shed", "hut", "hangar", "stadium", "warehouse", "kiosk", "bungalow", "farm" }},
                { 2,
                    new List<string>{ "semidetached_house", "house", "detached" }},
                { 3,
                    new List<string>{ "industrial", "train_station", "school", "kindergarten", "chapel", "cathedral", "church", "mosque", "religious", "synagogue", "temple", "shrine" }}
            },
                BuildingsTypesToEntrancesCount = new Dictionary<int, List<string>>()
            {
                { 1,
                    new List<string>{ "train_station", "kiosk", "bungalow", "cabin", "detached", "ger", "house", "houseboat", "static_caravan", "semidetached_house", "terrace", "chapel", "shed"}},
                { 2,
                    new List<string>{ "farm", "hospital", "kindergarten", "school", "university"}},
                { 3,
                    new List<string>{ "residential" , "apartments"}},
                { 4,
                    new List<string>{"industrial"}}
            },
                BuildingsTypesToFlatsCount = new Dictionary<int, List<string>>()
            {
                { 1,
                    new List<string>{ "bungalow", "detached", "farm", "terrace"}},
                { 4,
                    new List<string>{ "residential" , "apartments"}}
            },
                IsLevelsGenerationEnabled = true,
                IsEntrancesGenerationEnabled = true,
                IsFlatsGenerationEnabled = true
            };
        }
    }
}
