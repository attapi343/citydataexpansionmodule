﻿using OSMLSGlobalLibrary.Modules;
using OSMLSGlobalLibrary.Map;
using System.Linq;
using CityDataExpansionModule;
using CityDataExpansionModule.OsmGeometries;
using System;
using OSMLSGlobalLibrary.Observable.Geometries;
using OSMLSGlobalLibrary.Observable.Geometries.Actor;

namespace CityDataExpansionTestModule
{
    [CustomStyle(@"new style.Style({
                image: new style.Circle({
                    opacity: 1.0,
                    scale: 1.0,
                    radius: 3,
                    fill: new style.Fill({
                      color: 'rgba(255, 0, 0, 1)'
                    }),
                    stroke: new style.Stroke({
                      color: 'rgba(0, 0, 0, 1)',
                      width: 1
                    }),
                })
            });
        ")]
    public class Shop : PointActor
    {
        public Shop(OsmNode node) : base(node.Coordinate)
        {
            if (!node.Tags.Keys.Contains("shop"))
            {
                throw new Exception("Node is not a shop!");
            }
        }
    }

    [CustomStyle(@"new style.Style({
                image: new style.Circle({
                    opacity: 1.0,
                    scale: 1.0,
                    radius: 3,
                    fill: new style.Fill({
                      color: 'rgba(255, 0, 0, 0.9)'
                    }),
                    stroke: new style.Stroke({
                      color: 'rgba(0, 0, 0, 0.4)',
                      width: 1
                    }),
                })
            });
        ")]
    public class Entrances : PointActor
    {
        public Entrances(OsmNode node) : base(node.Coordinate)
        {
            if (!node.Tags.Keys.Contains("entrance"))
            {
                throw new Exception("Node is not a entrances!");
            }
        }
    }

    [CustomStyle(@"new style.Style({
                stroke: new style.Stroke({
                    color: 'rgba(0, 0, 255, 1)',
                    width: 3
                })
            });
        ")]
    public class Roads : LineStringActor
    {
        public Roads(OsmWay way) : base(way.Coordinates)
        {     
        }
    }

    [CustomStyle(@"new style.Style({
                fill: new style.Fill({
                    color: 'rgba(0, 0, 255, 0.1)'
                }),
                stroke: new style.Stroke({
                    color: 'rgba(255, 0, 0, 0.4)',
                    width: 1
                })
            });
        ")]
    public class Building : PolygonActor
    {
        public int Levels { get; }
        public Building(OsmClosedWay osmClosedWay) : base(new ObservableLinearRing(osmClosedWay.Coordinates))
        {
            if (!osmClosedWay.Tags.Keys.Contains("building"))
            {
                throw new Exception("Closed way is not a building!");
            }

            if (osmClosedWay.Tags.ContainsKey("building:levels"))
            {
                Levels = int.Parse(osmClosedWay.Tags["building:levels"]);
            }
        }
    }


    public class CityDataExpansionTest : OSMLSModule
    {

        public override void Update(long elapsedMilliseconds) { }

        protected override void Initialize()
        {
            MapObjects.Add(GetModule<CityDataExpansion>().MapBorders);

            var shops = MapObjects
                .GetAll<OsmNode>()
                .Where(x => x.Tags.ContainsKey("shop"))
                .Select(node => new Shop(node));

            //foreach (var shop in shops)
            //{
            //    MapObjects.Add(shop);
            //}

            var entrances = MapObjects
               .GetAll<OsmNode>()
               .Where(x => x.Tags.ContainsKey("entrance"))
               .Select(node => new Entrances(node));

            foreach (var entrance in entrances)
            {
                MapObjects.Add(entrance);
            }


            foreach (var buildingClosedWay in GetModule<CityDataExpansion>().AllBuildings)
            {
                MapObjects.Add(new Building(buildingClosedWay));
            }

            var roads = MapObjects
                .GetAll<OsmWay>()
                .Where(x => x.Tags.ContainsKey("highway"))
                .Select(way => new Roads(way));

            //foreach (var road in roads)
            //{
            //    MapObjects.Add(road);
            //}

        }
    }
}
