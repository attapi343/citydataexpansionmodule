﻿using CityDataExpansionModule;
using CityDataExpansionModule.Configuration;
using CityDataExpansionModule.OsmGeometries;
using NUnit.Framework;
using OSMLSGlobalLibrary;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTestCityDataExpansion
{
    class OsmObjectsGeneratorTest
    {
        [SetUp]
        public void Setup()
        {

        }

        private static GeneratorConfiguration GeneratorConfiguration =>
            new GeneratorConfiguration()
            {
                DefaultLevelsCount = 3,
                DefaultEntrancesCount = 2,
                DefaultFlatsCountForEveryEntranceLevels = 3,

                BuildingsTypesToLevelsCount = new Dictionary<int, List<string>>()
                {
                    { 2, new List<string>{"house"}},
                    { 6, new List<string>{ "residential"}}
                },
                BuildingsTypesToEntrancesCount = new Dictionary<int, List<string>>()
                {
                    { 1, new List<string>{ "house"}},
                    { 4, new List<string>{ "residential" }},
                },
                BuildingsTypesToFlatsCount = new Dictionary<int, List<string>>()
                {
                    {1, new List<string>{"house"}},
                    {4, new List<string>{"residential"}}
                },
                IsLevelsGenerationEnabled = true,
                IsEntrancesGenerationEnabled = true,
                IsFlatsGenerationEnabled = true
            };

        private static List<IOsmObject> OsmObjectsData
        {
            get
            {
                var firstBuildingOneNode = new OsmNode(MathExtensions.LatLonToSpherMerc(1, 4));
                var secondBuildingOneNode = new OsmNode(MathExtensions.LatLonToSpherMerc(3, 3));
                var thirdBuildingOneNode = new OsmNode(MathExtensions.LatLonToSpherMerc(3, 8));
                var fourthBuildingOneNode = new OsmNode(MathExtensions.LatLonToSpherMerc(1, 7));

                var firstBuildingTwoNode = new OsmNode(MathExtensions.LatLonToSpherMerc(5, 1));
                var secondBuildingTwoNode = new OsmNode(MathExtensions.LatLonToSpherMerc(6, 3));
                var thirdBuildingTwoNode = new OsmNode(MathExtensions.LatLonToSpherMerc(5, 3));

                var buildingOne = new OsmClosedWay(
                    new [] { firstBuildingOneNode, secondBuildingOneNode, thirdBuildingOneNode, fourthBuildingOneNode, firstBuildingOneNode },
                    new Dictionary<string, string>() { { "building", "residential" } }
                );
                var buildingTwo = new OsmClosedWay(
                    new [] { firstBuildingTwoNode, secondBuildingTwoNode, thirdBuildingTwoNode, firstBuildingTwoNode },
                    new Dictionary<string, string>() { { "building", "yes" } }
                );

                return new List<IOsmObject>() {
                    firstBuildingOneNode,
                    secondBuildingOneNode,
                    thirdBuildingOneNode,
                    fourthBuildingOneNode,
                    firstBuildingTwoNode,
                    secondBuildingTwoNode,
                    thirdBuildingTwoNode,
                    buildingOne,
                    buildingTwo
                };
            }
        }

        /// <summary>
        /// Тест проверяет работу генерацию этажей с использованием данных конфигурации.
        /// </summary>
        [Test]
        public void TestOsmLevelsGenerationOnConfiguration()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsEntrancesGenerationEnabled = false;
            generatorConfiguration.IsFlatsGenerationEnabled = false;
            const int expectedResult = 6;
            var osmObjectsData = OsmObjectsData;

            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);
            var actualData = osmObjectsData.Where(x => x.Tags.ContainsValue("residential")).Cast<OsmClosedWay>().First();

            Assert.IsTrue(int.Parse(actualData.Tags["building:levels"]) == expectedResult);
        }

        /// <summary>
        /// Тест проверяет работу генерацию этажей с взятием данных соседнего дома. 
        /// Условие выполняется в случае если данные конфигурации для такого типа здания не указаны и поблизости существует здание с тегом building:levels.
        /// </summary>
        [Test]
        public void TestOsmLevelsGenerationByBuildingNearby()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsEntrancesGenerationEnabled = false;
            generatorConfiguration.IsFlatsGenerationEnabled = false;
            const int expectedResult = 6;
            var osmObjectsData = OsmObjectsData;
  
            var buildingNearby = osmObjectsData.Where(x => x.Tags.ContainsValue("residential")).Cast<OsmClosedWay>().First();
            buildingNearby.Tags["building:levels"] = "6";

            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);
            var actualData = osmObjectsData.Where(x => x.Tags.ContainsValue("yes")).Cast<OsmClosedWay>().First();

            Assert.IsTrue(int.Parse(actualData.Tags["building:levels"]) == expectedResult);
        }

        /// <summary>
        /// Тест проверяет работу генерацию этажей по дефолтному значению.
        /// Условие выполняется в случае если данные конфигурации для такого типа здания не указаны и поблизости не существует здание с тегом building:levels.
        /// </summary>
        [Test]
        public void TestOsmLevelsGenerationWithUnspecifiedConfigurationAndNoBuildingNearby()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsEntrancesGenerationEnabled = false;
            generatorConfiguration.IsFlatsGenerationEnabled = false;
            const int expectedResult = 3;
            var osmObjectsData = OsmObjectsData;

            osmObjectsData.Remove(osmObjectsData.First(b => b.Tags.ContainsKey("building")));
            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);
            var actualData = osmObjectsData.Where(x => x.Tags.ContainsValue("yes")).Cast<OsmClosedWay>().First();

            Assert.IsTrue(int.Parse(actualData.Tags["building:levels"]) == expectedResult);
        }

        /// <summary>
        /// Тест проверяет работу генерацию подъездов с использованием данных конфигурации.
        /// </summary>
        [Test]
        public void TestOsmEntrancesGenerationOnConfiguration()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsLevelsGenerationEnabled = false;
            generatorConfiguration.IsFlatsGenerationEnabled = false;
            const int expectedResult = 4;
            var osmObjectsData = OsmObjectsData;

            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);
            var actualData = osmObjectsData.Where(x => x.Tags.ContainsKey("building")).Cast<OsmClosedWay>().First();
            Assert.IsTrue(actualData.Nodes.Count(n => n.Tags.ContainsKey("entrance")) == expectedResult);

        }

        /// <summary>
        /// Тест проверяет работу генерацию подъездов с использованием дефолтных данных.
        /// Условие выполняется в случае если данные конфигурации для такого типа здания не указаны.
        /// </summary>
        [Test]
        public void TestOsmEntrancesGenerationWithDefaultValues()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsLevelsGenerationEnabled = false;
            generatorConfiguration.IsFlatsGenerationEnabled = false;
            const int expectedResult = 2;
            var osmObjectsData = OsmObjectsData;

            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);
            var actualData = osmObjectsData.Where(x => x.Tags.ContainsKey("building")).Cast<OsmClosedWay>().Last();

            Assert.IsTrue(actualData.Nodes.Count(n => n.Tags.ContainsKey("entrance")) == expectedResult);

        }

        /// <summary>
        /// Тест проверяет работу генерацию квартир с использованием данных конфигурации.
        /// </summary>
        [Test]
        public void TestOsmFlatsGenerationOnConfiguration()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsLevelsGenerationEnabled = false;
            const int expectedResult = 96;
            var osmObjectsData = OsmObjectsData;

            var actualData = osmObjectsData.Where(x => x.Tags.ContainsKey("building")).Cast<OsmClosedWay>().First();
            actualData.Tags["building:levels"] = "6";
            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);

            Assert.IsTrue(int.Parse(actualData.Tags["building:flats"]) == expectedResult);
        }

        /// <summary>
        /// Тест проверяет работу генерацию подъездов с использованием дефолтных данных.
        /// Условие выполняется в случае если данные конфигурации для такого типа здания не указаны.
        /// </summary>
        [Test]
        public void TestOsmFlatsGenerationWithDefaultValues()
        {
            var generatorConfiguration = GeneratorConfiguration;
            generatorConfiguration.IsLevelsGenerationEnabled = false;
            const int expectedResult = 30;
            var osmObjectsData = OsmObjectsData;

            var actualData = osmObjectsData.Where(x => x.Tags.ContainsKey("building")).Cast<OsmClosedWay>().Last();
            actualData.Tags["building:levels"] = "5";
            new OsmObjectsGenerator().Generate(osmObjectsData, generatorConfiguration);

            Assert.IsTrue(int.Parse(actualData.Tags["building:flats"]) == expectedResult);
        }
    }
}
