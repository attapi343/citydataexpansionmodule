﻿using CityDataExpansionModule;
using CityDataExpansionModule.ObjectsXml;
using CityDataExpansionModule.OsmGeometries;
using NetTopologySuite.Geometries;
using NUnit.Framework;
using OSMLSGlobalLibrary;
using System.Collections.Generic;


namespace NUnitTestCityDataExpansion
{
    public class OsmObjectsConverterTest
    {
        [SetUp]
        public void Setup()
        {
        }

        private const string TagKey = "name";
        private const string SampleNodeTagValue = "Sample Node";
        private const string SampleWayTagValue = "Sample Way";
        private const string SampleRelationTagValue = "Sample node + way Relation";
        private const string SeparateNodeRole = "Separate Node Role";
        private const string WayRole = "Way Role";

        private OsmXml OsmXmlData { get; } = new OsmXml()
        {
            Bounds = new BoundsXml() { Minlat = "1", Minlon = "1", Maxlat = "6", Maxlon = "5" },
            Node = new List<NodeXml>()
            {
                new NodeXml(){ Id="1", Lat="1", Lon="1", Tag = new List<TagXml>()},
                new NodeXml(){ Id="2", Lat="2", Lon="5", Tag = new List<TagXml>()},
                new NodeXml(){ Id="3", Lat="6", Lon="4", Tag = new List<TagXml>(){
                    new TagXml() { K = TagKey, V = SampleNodeTagValue}
                } },
                new NodeXml(){ Id="4", Lat="6", Lon="1", Tag = new List<TagXml>()}
            },
            Way = new List<WayXml>()
            {
                new WayXml(){ Id = "5", Nd = new List<NdXml>{
                    new NdXml() { Ref="1"},
                    new NdXml() { Ref="2"},
                    new NdXml() { Ref="4"},
                    new NdXml() { Ref="1"},
                },
                    Tag = new List<TagXml>(){
                        new TagXml() { K = TagKey, V = SampleWayTagValue}
                    }
                }
            },
            Relation = new List<RelationXml>()
            {
                new RelationXml(){ Id="6" , Member= new List<MemberXml>{
                    new MemberXml(){ Type="node", Ref="3", Role=SeparateNodeRole},
                    new MemberXml(){ Type="way", Ref="5", Role="" }
                },
                    Tag = new List<TagXml>(){
                        new TagXml() { K = TagKey, V = SampleRelationTagValue}
                    }
                }
            }
        };

        private static List<IOsmObject> ExpectedOsmObjects
        {
            get
            {
                var firstWayNode = new OsmNode(MathExtensions.LatLonToSpherMerc(1.0, 1.0));
                var secondWayNode = new OsmNode(MathExtensions.LatLonToSpherMerc(2, 5));
                var thirdWayNode = new OsmNode(MathExtensions.LatLonToSpherMerc(6, 1));

                var separateNode = new OsmNode(MathExtensions.LatLonToSpherMerc(6, 4), new Dictionary<string, string>() {
                    { TagKey, SampleNodeTagValue }
                });

                var way = new OsmWay(
                    new [] { firstWayNode, secondWayNode, thirdWayNode, firstWayNode },
                    new Dictionary<string, string>() { { TagKey, SampleWayTagValue } }
                );

                var relation = new OsmRelation(new List<(Geometry geometry, string role)>()
                    {
                        (separateNode, SeparateNodeRole),
                        (way, WayRole)
                    },
                    new Dictionary<string, string>() { { TagKey, SampleRelationTagValue } }
                );

                return new List<IOsmObject>() {
                    firstWayNode,
                    secondWayNode,
                    separateNode,
                    thirdWayNode,
                    way,
                    relation
                };
            }
        }

        [Test]
        public void TestOsmXmlConvertation()
        {
            var (_, actualOsmObjects) = new OsmObjectsConverter().ConvertObjectsOSM(OsmXmlData);

            for (var i = 0; i < ExpectedOsmObjects.Count; i++)
            {
                var expectedPoint = (Geometry)ExpectedOsmObjects[i];
                var actualPoint = (Geometry)actualOsmObjects[i];

                Assert.IsTrue(expectedPoint == actualPoint);
            }
        }
    }
}